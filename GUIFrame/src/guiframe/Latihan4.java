/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiframe;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author pyoga
 */
public class Latihan4 extends JFrame{
    
    public Latihan4() {
        this.setSize(500, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        
        setLayout(null);
         
        JTextField tf1 = new JTextField();
        JButton btn1 = new JButton("Find");
        JLabel lb1 = new JLabel("Keyword : ");
        lb1.setBounds(50, 20, 400, 20);
        tf1.setBounds(50, 50, 400, 20);
        btn1.setBounds(200, 80, 100, 20);
        
        add(lb1);
        add(tf1);
        add(btn1);
    }
     public static void main(String[] args) {
        // TODO code application logic here
        new Latihan4();
    }
}
