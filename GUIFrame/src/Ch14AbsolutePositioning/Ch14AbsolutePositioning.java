/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ch14AbsolutePositioning;

import java.awt.Color;
import java.awt.Container;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author pyoga
 */
class Ch14AbsolutePositioning extends JFrame {
    
    private static final int FRAME_WIDTH       = 400;
    private static final int FRAME_HEIGHT      = 400;
    private static final int FRAME_X_ORIGIN    = 300;
    private static final int FRAME_Y_ORIGIN    = 100;
    private static final int BUTTON_WIDTH      = 80;
    private static final int BUTTON_HEIGHT     = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txt;
    private JLabel nama;
    private JLabel gender;
    private JLabel hobi;
    private JRadioButton lakiRB;
    private JRadioButton perempuanRB;
    private JCheckBox olahragaCB;
    private JCheckBox shoppingCB;
    private JCheckBox gamesCB;
    private JCheckBox nontonCB;
    
    
    
    public static void main(String[] args) {
        Ch14AbsolutePositioning frame = new Ch14AbsolutePositioning();
        frame.setVisible(true);
    }
    
    public Ch14AbsolutePositioning() {
        Container contentPane = getContentPane();
        
        setSize      (FRAME_WIDTH, FRAME_HEIGHT);
        setResizable (true);
        setTitle     ("Program Input Data");
        setLocation  (FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
         nama = new JLabel ("Nama : ");
        nama.setBounds(10, 10, 250, 20);
        contentPane.add(nama);
        
         txt = new JTextField ();
        txt.setBounds(115, 8, 250, 20);
        contentPane.add(txt);
        
        gender = new JLabel ("jenis Kelamin : ");
        gender.setBounds(10, 40, 90, 10);
        contentPane.add(gender);
        
        lakiRB = new JRadioButton("laki - laki");
        lakiRB.setBounds(110, 36, 85, 20);
        contentPane.add(lakiRB);
        perempuanRB = new JRadioButton("perempuan");
        perempuanRB.setBounds(200, 36, 91, 20);
        contentPane.add(perempuanRB);
        
        ButtonGroup group = new ButtonGroup();
        group.add(lakiRB);
        group.add(perempuanRB);
        
        hobi = new JLabel("Hobi: ");
        hobi.setBounds(10,60,250,20);
        contentPane.add(hobi);
        
        olahragaCB = new JCheckBox("olahraga");
        olahragaCB.setBounds(110,60,250,20);
        contentPane.add(olahragaCB);
        
        shoppingCB = new JCheckBox("Shopping");
        shoppingCB.setBounds(110,80,250,20);
        contentPane.add(shoppingCB);
        gamesCB = new JCheckBox("Games");
        gamesCB.setBounds(110,100,250,20);
        contentPane.add(gamesCB);
        nontonCB = new JCheckBox("Nonton");
        nontonCB.setBounds(110,120,250,20);
        contentPane.add(nontonCB);
        
        okButton = new JButton("OK");
        okButton.setBounds(113, 165, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(180, 165, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        
        
        
//        label = new JLabel ("Nama : ");
//        nama.setBounds(10, 40, 250, 20);
//        contentPane.add(gender);
//        
//        okButton = new JButton ("OK");
//        okButton.setBounds(100, 150, BUTTON_WIDTH, BUTTON_HEIGHT);
//        contentPane.add(okButton);
//        
//        cancelButton = new JButton ("CANCEL");
//        cancelButton.setBounds(200, 150, BUTTON_WIDTH, BUTTON_HEIGHT);
//        contentPane.add(cancelButton);
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
