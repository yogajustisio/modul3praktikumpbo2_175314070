/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ch14AbsolutePositioning;

import java.awt.Color;
import java.awt.Container;
//import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JTextField;
/**
 *
 * @author pyoga
 */
public class Dialog extends JFrame {
   private static final int FRAME_WIDTH       = 400;
    private static final int FRAME_HEIGHT      = 400;
    private static final int FRAME_X_ORIGIN    = 300;
    private static final int FRAME_Y_ORIGIN    = 100;
    private static final int BUTTON_WIDTH      = 80;
    private static final int BUTTON_HEIGHT     = 40;
    
    private JPanel panel;
    private JLabel label;
      public static void main(String[] args) {
       Dialog frame = new Dialog();
        frame.setVisible(true);
      }
    public  Dialog() {
//        setSize(400, 200);
//        setLocation(50, 50);
//        contentPane = getContentPane();
//        contentPane.setLayout(new GridLayout (2,1));
        Container contentPane = getContentPane();
        
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable (true);
        setTitle     ("Program Ch14AbsolutePositioning");
        setLocation  (FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        panel = new JPanel();
//        panel.setLayout(new GridLayout (4,3));
        
        label = new JLabel ("Nama : ");
        panel.add(label);
        
         setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
