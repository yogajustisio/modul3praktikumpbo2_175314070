/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ch14AbsolutePositioning;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author pyoga
 */
class Ch14ListSample extends JFrame {
    
    private static final int FRAME_WIDTH    = 300;
    private static final int FRAME_HEIGHT   = 250;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JList list;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Ch14ListSample frame = new Ch14ListSample();
        frame.setVisible(true);
        
    }
    public Ch14ListSample() {
        Container   contentPane;
        JPanel      listPanel, okPanel;
        
        JButton     okButton;
        String[]    names = {"Ape", "Bat", "Bee", "Cat",
                             "Dog", "Eel", "Fox", "Gnu",
                             "Hen", "Man", "Sow", "Yak"};
        
        setSize (FRAME_WIDTH, FRAME_HEIGHT);
        setTitle ("Program Ch14JListSample2");
        setLocation (FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane = getContentPane();
        contentPane.setBackground(Color.white);
        contentPane.setLayout(new BorderLayout());
        
        listPanel = new JPanel (new GridLayout(0, 1));
        listPanel.setBorder (BorderFactory.createTitledBorder("Three-Letter Animal Names"));
        
        list = new JList (names);
        listPanel.add(new JScrollPane(list));
        
        okPanel = new JPanel (new FlowLayout());
        okButton = new JButton ("OK");
        okPanel.add(okButton);
        
        contentPane.add(listPanel, BorderLayout.CENTER);
        contentPane.add(okPanel, BorderLayout.SOUTH);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
}
